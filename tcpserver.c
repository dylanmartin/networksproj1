/* tcpserver.c */
/* Programmed by Adarsh Sethi */
/* Sept. 19, 2019 */

#include <ctype.h>      /* for toupper */
#include <stdio.h>      /* for standard I/O functions */
#include <stdlib.h>     /* for exit */
#include <string.h>     /* for memset */
#include <sys/socket.h> /* for socket, bind, listen, accept */
#include <netinet/in.h> /* for sockaddr_in */
#include <unistd.h>     /* for close */

#define STRING_SIZE 1024

/* SERV_TCP_PORT is the port number on which the server listens for
   incoming requests from clients. You should change this to a different
   number to prevent conflicts with others in the class. */

#define SERV_TCP_PORT 65000

// keeps track of the next expected packet
int expectedPacket = 1;
// keeps track of total bytes recieved
int bytecount = 0;
/* setting up header format */
typedef struct PktHeader
{
   unsigned short count;
   unsigned short seq;
} PktHeader_t;

/* declare functions for header */
PktHeader_t *new_pkt(unsigned short, unsigned short);
void deletepkt(PktHeader_t *);

PktHeader_t *new_pkt(unsigned short count, unsigned short seq)
{
   PktHeader_t *new_pkt = (PktHeader_t *)malloc(sizeof(PktHeader_t));
   new_pkt->count = count;
   new_pkt->seq = seq;
   return new_pkt;
}
void deletepkt(PktHeader_t *p)
{
   free(p);
}

int main(void)
{
   // temporary until headers implemented
   int packetNum = 1;

   int sock_server;     /* Socket on which server listens to clients */
   int sock_connection; /* Socket on which server exchanges data with client */
   const char *terminate = "End of Transmission Packet\0";
   struct sockaddr_in server_addr; /* Internet address structure that
                                        stores server address */
   unsigned int server_addr_len;   /* Length of server address structure */
   unsigned short server_port;     /* Port number used by server (local port) */

   struct sockaddr_in client_addr; /* Internet address structure that
                                        stores client address */
   unsigned int client_addr_len;   /* Length of client address structure */

   char sentence[STRING_SIZE];         /* receive message */
   char modifiedSentence[STRING_SIZE]; /* send message */
   unsigned int msg_len;               /* length of message */
   int bytes_sent, bytes_recd;         /* number of bytes sent or received */
   unsigned int i;                     /* temporary loop variable */

   /* open a socket */

   if ((sock_server = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
   {
      perror("Server: can't open stream socket");
      exit(1);
   }

   /* initialize server address information */

   memset(&server_addr, 0, sizeof(server_addr));
   server_addr.sin_family = AF_INET;
   server_addr.sin_addr.s_addr = htonl(INADDR_ANY); /* This allows choice of
                                        any host interface, if more than one
                                        are present */
   server_port = SERV_TCP_PORT;                     /* Server will listen on this port */
   server_addr.sin_port = htons(server_port);

   /* bind the socket to the local server port */

   if (bind(sock_server, (struct sockaddr *)&server_addr,
            sizeof(server_addr)) < 0)
   {
      perror("Server: can't bind to local address");
      close(sock_server);
      exit(1);
   }

   /* listen for incoming requests from clients */

   if (listen(sock_server, 50) < 0)
   {                                     /* 50 is the max number of pending */
      perror("Server: error on listen"); /* requests that will be queued */
      close(sock_server);
      exit(1);
   }
   printf("I am here to listen ... on port %hu\n\n", server_port);

   client_addr_len = sizeof(client_addr);

   /* wait for incoming connection requests in an indefinite loop */

   for (;;)
   {

      // erase previously held value in modifiedSentence
      bzero(modifiedSentence, STRING_SIZE);

      sock_connection = accept(sock_server, (struct sockaddr *)&client_addr,
                               &client_addr_len);
      /* The accept function blocks the server until a
                        connection request comes from a client */
      printf("connection Accepted");
      if (sock_connection < 0)
      {
         perror("Server: accept() error\n");
         close(sock_server);
         exit(1);
      }

      /* receive the message */
      PktHeader_t *pkt = (PktHeader_t *)malloc(sizeof(PktHeader_t));
      bytes_recd = recv(sock_connection, pkt, sizeof(PktHeader_t), 0);

      if (bytes_recd > 0)
      {
         // open file
         FILE *fp;
         char str[STRING_SIZE];
         char *filename = "./output.txt";
         fp = fopen(filename, "w");
         if (fp == NULL)
         {
            printf("Could not open file %s", filename);
         }
         do
         {
            bytes_recd = recv(sock_connection, sentence, STRING_SIZE, 0);
            msg_len = bytes_recd;
            if (strcmp(sentence, terminate) == 0)
            {
               // final packet recieved print message
               printf("End of Transmission Packet with sequence number %d received with %u data bytes\n", pkt->seq, pkt->count);
               bytes_sent = send(sock_connection, "Closed", strlen("Closed") + 1, 0);
               bytecount += pkt->count;
               printf("recieved %d packets and %d bytes of data\n", expectedPacket, bytecount);
               expectedPacket = 1;
               bytecount = 0;
            }
            else
            {
               printf("Packet %d received with %u data bytes\n", pkt->seq, pkt->count);
               /* prepare the message to send */
               for (i = 0; i < msg_len; i++)
                  modifiedSentence[i] = toupper(sentence[i]);

               // write line to file
               fputs(modifiedSentence, fp);

               /* send message */
               //send header first
               PktHeader_t *pktback = new_pkt(msg_len, pkt->seq);
               bytes_sent = send(sock_connection, pktback, sizeof(pktback), 0);
               bytes_sent = send(sock_connection, modifiedSentence, msg_len, 0);
               packetNum++;
               bytecount += pkt->count;
               expectedPacket++;
            }
         } while ((bytes_recd = recv(sock_connection, pkt, sizeof(PktHeader_t), 0)) > 0);
         fclose(fp);
      }
      /* close the socket */
      close(sock_connection);
      // kill after recieves data for debuging purposes
      // exit(1);
   }
}
