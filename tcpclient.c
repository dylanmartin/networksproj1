/* tcp_ client.c */
/* Programmed by Adarsh Sethi */
/* Sept. 19, 2019 */

#include <stdio.h>      /* for standard I/O functions */
#include <stdlib.h>     /* for exit */
#include <string.h>     /* for memset, memcpy, and strlen */
#include <netdb.h>      /* for struct hostent and gethostbyname */
#include <sys/socket.h> /* for socket, connect, send, and recv */
#include <netinet/in.h> /* for sockaddr_in */
#include <unistd.h>     /* for close */

#define STRING_SIZE 1024
  

/* setting up header format */
typedef struct PktHeader
{
   unsigned short count;
   unsigned short seq;
} PktHeader_t;

/* declare functions for header */
PktHeader_t *new_pkt(unsigned short, unsigned short);
void deletepkt(PktHeader_t *);

PktHeader_t *new_pkt(unsigned short count, unsigned short seq)
{
   PktHeader_t *new_pkt = (PktHeader_t *)malloc(sizeof(PktHeader_t));
   new_pkt->count = count;
   new_pkt->seq = seq;
   return new_pkt;
}
void deletepkt(PktHeader_t *p)
{
   free(p);
}

int main(void)
{
   // Number for keeping track of the number of packets sent
   int packetCount = 1;

   int sock_client; /* Socket used by client */

   struct sockaddr_in server_addr;    /* Internet address structure that
                                        stores server address */
   struct hostent *server_hp;         /* Structure to store server's IP
                                        address */
   char server_hostname[STRING_SIZE]; /* Server's hostname */
   unsigned short server_port;        /* Port number used by server (remote port) */

   char sentence[STRING_SIZE];         /* send message */
   char modifiedSentence[STRING_SIZE]; /* receive message */
   unsigned int msg_len;               /* length of message */
   int bytes_sent, bytes_recd;         /* number of bytes sent or received */

   char pktnum[16], databytes[16]; /* packet number and data bytes taken from the header  */
   char header[STRING_SIZE];
   /* open a socket */

   if ((sock_client = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
   {
      perror("Client: can't open stream socket");
      exit(1);
   }

   /* Note: there is no need to initialize local client address information 
            unless you want to specify a specific local port
            (in which case, do it the same way as in udpclient.c).
            The local address initialization and binding is done automatically
            when the connect function is called later, if the socket has not
            already been bound. */

   /* initialize server address information */

   printf("Enter hostname of server: ");
   scanf("%s", server_hostname);
   if ((server_hp = gethostbyname(server_hostname)) == NULL)
   {
      perror("Client: invalid server hostname");
      close(sock_client);
      exit(1);
   }

   printf("Enter port number for server: ");
   scanf("%hu", &server_port);

   /* Clear server address structure and initialize with server address */
   memset(&server_addr, 0, sizeof(server_addr));
   server_addr.sin_family = AF_INET;
   memcpy((char *)&server_addr.sin_addr, server_hp->h_addr,
          server_hp->h_length);
   server_addr.sin_port = htons(server_port);

   /* connect to the server */

   if (connect(sock_client, (struct sockaddr *)&server_addr,
               sizeof(server_addr)) < 0)
   {
      perror("Client: can't connect to server");
      close(sock_client);
      exit(1);
   }

   // open file
   FILE *fp;
   char str[STRING_SIZE];
   char *filename = "./test2.txt";

   fp = fopen(filename, "r");
   if (fp == NULL)
   {
      printf("Could not open file %s", filename);
      return 1;
   }
   // Read every line of the file and store it in the variable str
   while (fgets(str, STRING_SIZE, fp) != NULL)
   {
      msg_len = strlen(str) + 1;

      PktHeader_t *header = new_pkt((unsigned short)msg_len, packetCount);

      /* send message */
      bytes_sent = send(sock_client, header, sizeof(PktHeader_t), 0); //sending header

      bytes_sent = send(sock_client, str, msg_len, 0);
      printf("Packet %d transmitted with %d data bytes\n", header->seq, header->count);

      // increase the packet count
      packetCount++;
      usleep(100);
     
      /* get response from server */
      bytes_recd = recv(sock_client, modifiedSentence, STRING_SIZE, 0);
   }
   msg_len = strlen("End of Transmission Packet") + 1;
   // Send closing packet
   PktHeader_t *closingHeader = new_pkt((unsigned short)msg_len, packetCount);

   /* send message */
   bytes_sent = send(sock_client, closingHeader, sizeof(PktHeader_t), 0);
   //sending header
   bytes_sent = send(sock_client, "End of Transmission Packet\0", msg_len, 0);
   printf("End of Transmission Packet with sequence number %d transmitted with %u data bytes\n", packetCount, msg_len);
   // get response from server
   bytes_recd = recv(sock_client, modifiedSentence, STRING_SIZE, 0);

   /* close the socket */
   close(sock_client);
   fclose(fp);
}
