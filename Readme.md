# TCP Client and Server

## Netowrks Project 1

### By Dylan Martin and Dylan Knox

## Instilation:

To extract the file run the following command

```bash
tar -xzvf proj1.tar.gz
```

## Usage:

To build and run the server and open up two different terminal windows, one for the server and another for the client, and in both cd into the folder you just unzipped in the instilation process. In one window run the following command to build and start the server.

```bash
./createAndRunServer.sh
```

In the other window run the following command to build and run the client.

```bash
./createAndRunClient.sh
```

Add the name of the host you want to connect to (localhost), and the port you want to connect to (65000 by default). Then the client will send over the data from test2.txt to the server and the server will then pipe the modified output to output.txt